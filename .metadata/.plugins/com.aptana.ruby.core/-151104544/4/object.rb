class Object < BasicObject
  include Kernel

  APPLE_GEM_HOME = "/System/Library/Frameworks/Ruby.framework/Versions/2.0/usr/lib/ruby/gems/2.0.0"
  ARGF = ARGF
  ARGV = []
  Addrinfo = Addrinfo
  ArgumentError = ArgumentError
  Array = Array
  BasicObject = BasicObject
  BasicSocket = BasicSocket
  Bignum = Bignum
  Binding = Binding
  CGI = CGI
  CROSS_COMPILING = nil
  Class = Class
  Comparable = Comparable
  Complex = Complex
  ConditionVariable = ConditionVariable
  Config = RbConfig::Obsolete
  Data = Data
  Date = Date
  Delegator = Delegator
  Digest = Digest
  Dir = Dir
  ENV = {"PATH"=>"/Users/u268/.npm-packages/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/git/bin:/Users/u268/alf/ci/node-v0.10.32-darwin-x64/bin:/Users/u268/stash/LIFE/alf/ci/node-v0.10.32-darwin-x64/bin", "IRBRC"=>"/Users/u268/.rvm/rubies/ruby-2.2.0/.irbrc", "_system_arch"=>"x86_64", "JAVA_STARTED_ON_FIRST_THREAD_47550"=>"1", "rvm_bin_path"=>"/Users/u268/.rvm/bin", "JAVA_HOME"=>"/Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home", "rvm_prefix"=>"/Users/u268", "LANG"=>"en_US.UTF-8", "APP_ICON_47550"=>"../Resources/Eclipse.icns", "rvm_loaded_flag"=>"1", "LOGNAME"=>"u268", "rvm_version"=>"1.26.10 (latest)", "XPC_SERVICE_NAME"=>"org.eclipse.platform.ide.61944", "PWD"=>"/Users/u268/Documents/_Shape Security/_Shape_Lab/Utilities/eclipse/Eclipse.app/Contents/MacOS", "rvm_user_install_flag"=>"1", "SHELL"=>"/bin/bash", "APTANA_VERSION"=>"3.4.2.1368863613", "_system_type"=>"Darwin", "MY_RUBY_HOME"=>"/Users/u268/.rvm/rubies/ruby-2.2.0", "_system_version"=>"10.10", "USER"=>"u268", "GEM_HOME"=>"/Users/u268/.rvm/gems/ruby-2.2.0", "TMPDIR"=>"/var/folders/9d/4vlr97qj343_v4kn8v_4y49w0000gp/T/", "rvm_path"=>"/Users/u268/.rvm", "rvm_stored_umask"=>"0022", "SSH_AUTH_SOCK"=>"/private/tmp/com.apple.launchd.jZ7fmXMYZf/Listeners", "XPC_FLAGS"=>"0x0", "GEM_PATH"=>"/Users/u268/.rvm/gems/ruby-2.2.0:/Users/u268/.rvm/gems/ruby-2.2.0@global", "RUBY_VERSION"=>"ruby-2.2.0", "_system_name"=>"OSX", "__CF_USER_TEXT_ENCODING"=>"0x1F6:0x0:0x0", "Apple_PubSub_Socket_Render"=>"/private/tmp/com.apple.launchd.2tY1IyYveA/Render", "HOME"=>"/Users/u268", "SHLVL"=>"1"}
  EOFError = EOFError
  Encoding = Encoding
  EncodingError = EncodingError
  Enumerable = Enumerable
  Enumerator = Enumerator
  Errno = Errno
  Etc = Etc
  Exception = Exception
  FALSE = false
  FalseClass = FalseClass
  Fcntl = Fcntl
  Fiber = Fiber
  FiberError = FiberError
  File = File
  FileTest = FileTest
  FileUtils = FileUtils
  Fixnum = Fixnum
  Float = Float
  FloatDomainError = FloatDomainError
  GC = GC
  Gem = Gem
  Hash = Hash
  IO = IO
  IOError = IOError
  IPSocket = IPSocket
  IndexError = IndexError
  Integer = Integer
  Interrupt = Interrupt
  Kernel = Kernel
  KeyError = KeyError
  LoadError = LoadError
  LocalJumpError = LocalJumpError
  Marshal = Marshal
  MatchData = MatchData
  Math = Math
  Method = Method
  Module = Module
  Monitor = Monitor
  MonitorMixin = MonitorMixin
  Mutex = Mutex
  NIL = nil
  NameError = NameError
  Net = Net
  NilClass = NilClass
  NoMemoryError = NoMemoryError
  NoMethodError = NoMethodError
  NotImplementedError = NotImplementedError
  Numeric = Numeric
  OUTPUT_PATH = "/Users/u268/web_app/blog/.metadata/.plugins/com.aptana.ruby.core/-151104544/4/"
  Object = Object
  ObjectSpace = ObjectSpace
  OpenSSL = OpenSSL
  OptionParser = OptionParser
  Proc = Proc
  Process = Process
  Queue = Queue
  RUBY_COPYRIGHT = "ruby - Copyright (C) 1993-2014 Yukihiro Matsumoto"
  RUBY_DESCRIPTION = "ruby 2.0.0p481 (2014-05-08 revision 45883) [universal.x86_64-darwin14]"
  RUBY_ENGINE = "ruby"
  RUBY_FRAMEWORK = true
  RUBY_FRAMEWORK_VERSION = "2.0.0"
  RUBY_PATCHLEVEL = 481
  RUBY_PLATFORM = "universal.x86_64-darwin14"
  RUBY_RELEASE_DATE = "2014-05-08"
  RUBY_REVISION = 45883
  RUBY_VERSION = "2.0.0"
  Random = Random
  Range = Range
  RangeError = RangeError
  Rational = Rational
  RbConfig = RbConfig
  Regexp = Regexp
  RegexpError = RegexpError
  Resolv = Resolv
  RubyVM = RubyVM
  RuntimeError = RuntimeError
  STDERR = IO.new
  STDIN = IO.new
  STDOUT = IO.new
  ScriptError = ScriptError
  SecureRandom = SecureRandom
  SecurityError = SecurityError
  Signal = Signal
  SignalException = SignalException
  SimpleDelegator = SimpleDelegator
  SizedQueue = SizedQueue
  Socket = Socket
  SocketError = SocketError
  StandardError = StandardError
  StopIteration = StopIteration
  String = String
  StringIO = StringIO
  Struct = Struct
  Symbol = Symbol
  SyntaxError = SyntaxError
  SystemCallError = SystemCallError
  SystemExit = SystemExit
  SystemStackError = SystemStackError
  TCPServer = TCPServer
  TCPSocket = TCPSocket
  TOPLEVEL_BINDING = #<Binding:0x007f9daa8a4838>
  TRUE = true
  TSort = TSort
  Tempfile = Tempfile
  Thread = Thread
  ThreadError = ThreadError
  ThreadGroup = ThreadGroup
  Time = Time
  Timeout = Timeout
  TimeoutError = Timeout::Error
  TracePoint = TracePoint
  TrueClass = TrueClass
  TypeError = TypeError
  UDPSocket = UDPSocket
  UNIXServer = UNIXServer
  UNIXSocket = UNIXSocket
  URI = URI
  UnboundMethod = UnboundMethod
  ZeroDivisionError = ZeroDivisionError
  Zlib = Zlib



  protected


  private

  def DelegateClass(arg0)
  end

  def Digest(arg0)
  end

  def dir_names(arg0)
  end

  def file_name(arg0)
  end

  def get_classes
  end

  def grab_instance_method(arg0, arg1)
  end

  def print_args(arg0)
  end

  def print_instance_method(arg0, arg1)
  end

  def print_method(arg0, arg1, arg2, arg3, arg4, *rest)
  end

  def print_type(arg0)
  end

  def print_value(arg0)
  end

  def timeout(arg0, arg1, arg2, *rest)
  end

end
